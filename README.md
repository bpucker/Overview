# Github Repository Overview
This is an overview of all github repositories associated with [PuckerLab](https://www.tu-braunschweig.de/en/ifp/pbb). Please find a list of the science communication projects, the developed bioinformatics tools, teaching activities, and project specific repositories below.


## (1) Science Communication
### (1.1) MybMonday
Specific facts and research projects about MYB transcription factors are shared on a weekly basis via Twitter and LinkedIn using [#MybMonday](https://twitter.com/search?q=%23MybMonday&src=typed_query). All posts are collected in the [#MybMonday github repository](https://github.com/bpucker/MybMonday).

<a href="https://github.com/bpucker/MybMonday">
<img alt="Phylogenetic tree of banana MYBs (Pucker <i>et al.</i>, 2020; doi:10.1371/journal.pone.0239275)" src="https://www.biorxiv.org/content/biorxiv/early/2020/02/03/2020.02.03.932046/F2.large.jpg" width="30%" height="30%">
</a>

### (1.2) FlavonoidFriday
Specific facts and research projects about the flavonoid biosynthesis are shared on a weekly basis via Twitter and LinkedIn using [#FlavonoidFriday](https://twitter.com/search?q=%23FlavonoidFriday&src=typed_query&f=top). All posts are collected in the [#FlavonoidFriday github repository](https://github.com/bpucker/FlavonoidFriday).

<a href="https://github.com/bpucker/FlavonoidFriday">
<img alt="Flavonoid biosynthesis pathway overview (Pucker <i>et al.,</i> 2020; doi:10.3390/plants9091103" src="https://www.mdpi.com/plants/plants-09-01103/article_deploy/html/images/plants-09-01103-g001.png" width="25%" height="25%">
</a>  


## (2) Tools
### (2.1) Knowledge-based Identification of Pathway Enzymes (KIPEs)
Knowledge-based Identification of Pathway Enzymes (KIPEs) performs an automatic annotation of the flavonoid biosynthesis steps in a new transcriptome of genome sequence assembly. Scripts and datasets are availabe in the [KIPEs repository](https://github.com/bpucker/KIPEs). KIPEs is also available on [our web server](http://pbb.bot.nat.tu-bs.de/KIPEs/). This enables all scientists to use KIPEs in their research projects. Please find additional details in the corresponding publication ['Automatic Identification of Players in the Flavonoid Biosynthesis with Application on the Biomedicinal Plant <i>Croton tiglium</i>'](https://www.mdpi.com/2223-7747/9/9/1103/htm). Recently, we updated KIPEs with additional features and data sets as described in our preprint ['KIPEs3: Automatic annotation of biosynthesis pathways'](https://doi.org/10.1101/2022.06.30.498365).

<a href="https://github.com/bpucker/KIPEs">
<img alt="KIPEs workflow (Pucker <i>et al.,</i> 2020; doi:10.3390/plants9091103; Rempel&Pucker,2022; doi:10.1101/2022.06.30.498365)" src="https://www.biorxiv.org/content/biorxiv/early/2022/07/02/2022.06.30.498365/F1.large.jpg?width=800&height=600&carousel=1" width="25%" height="25%">
</a>  

### (2.2) MYB_annotator
This tool performs an automatic identification, annotation, and analysis of the MYB gene family in plants. It can be applied to new transcriptome of genome assemblies. Please find the scripts and data sets in the [MYB_annotator repository](https://github.com/bpucker/MYB_annotator). The MYB_annotator is also available on [our web server](http://pbb.bot.nat.tu-bs.de/MYB_annotator/). This enables all scientists to use KIPEs in their research projects. Please find additional details in the corresponding publication ['Automatic identification and annotation of MYB gene family members in plants'](https://doi.org/10.1186/s12864-022-08452-5).

<a href="https://github.com/bpucker/MYB_annotator">
<img alt="MYB_annotator workflow (Pucker, 2022; doi:10.1186/s12864-022-08452-5)" src="https://media.springernature.com/full/springer-static/image/art%3A10.1186%2Fs12864-022-08452-5/MediaObjects/12864_2022_8452_Fig1_HTML.png?as=webp" width="25%" height="25%">
</a>


### (2.3) Mapping-based Genome Size Estimation (MGSE)
Mapping-based Genome Size Estimation (MGSE) performs an estimation of a genome size based on a read mapping to an existing genome sequence assembly. Please find the script and detailed instructions in the [MGSE repository](https://github.com/bpucker/MGSE). Additional details are described in the corresponding publication ['Mapping-based genome size estimation'](https://doi.org/10.1101/607390).

<a href="https://github.com/bpucker/MGSE">
<img alt="MGSE concept (Pucker, 2021; doi:10.1101/607390)" src="https://raw.githubusercontent.com/bpucker/MGSE/df459458173ec0bcffba7ce61484e5765118c817/MGSE_concept.png" width="50%" height="50%">
</a>


### (2.4) Neighborhood-Aware Variant Impact Predictor (NAVIP)
Prediction of the functional consequences of sequence variants listed in a VCF file. Please find scripts and detailed instructions in the [NAVIP repository](https://github.com/bpucker/NAVIP). We are currently working to make NAVIP available through our web server. Please find additional details in the corresponding publication ['Influence of neighboring small sequence variants on functional impact prediction'](https://github.com/bpucker/NAVIP).

<a href="https://github.com/bpucker/NAVIP">
<img alt="cInDels detected by NAVIP (Baasner <i>et al</i>., 2019; doi:10.1101/596718 )" src="https://www.biorxiv.org/content/biorxiv/early/2019/06/27/596718/F3.large.jpg?width=800&height=600&carousel=1" width="30%" height="30%">
</a>

## (3) Teaching @ Uni Bielefeld

### (3.1) Applied Python Programming for Life Scientists (APPLS)
This course enables life scientists to learn Python basics to address their specific research questions. The [APPLS repository](https://github.com/bpucker/APPLS) contains the materials of this course.

<a href="https://github.com/bpucker/APPLS">
<img alt="Applied Python Programming for Life Scientists (APPLS)" src="https://github.com/bpucker/figures/raw/main/Python_course.png" width="30%" height="30%">
</a>

### (3.2) Applied Plant Genomics
This course provides a comprehensive introduction to the analysis of next generation sequencing (NGS) data sets. All materials are available in the [AGR repository](https://github.com/bpucker/AppliedGenomeResearch).

<a href="https://github.com/bpucker/AppliedGenomeResearch">
<img alt="Applied Plant Genomics" src="https://github.com/bpucker/figures/raw/main/AppliedGenomeReserachCourseContentOverview.png" width="30%" height="30%">
</a>

### (3.3) Molecular Methods in Genome Research
This course introduces molecular biologists to wet lab methods of genome research. Please find the materials in the [MMGR repository](https://github.com/bpucker/MolecularMethodsInGenomeResearch).

<a href="https://github.com/bpucker/MolecularMethodsInGenomeResearch">
<img alt="Molecular Methods in Genome Research" src="https://github.com/bpucker/figures/raw/main/MolecularMethodsInGenomeReserachCourseOverview.png" width="30%" height="30%">
</a>

## (4) Teaching @ TU Braunschweig
### (4.1) Introduction to Biochemistry and Bioinformatics of Plants
Material used in this course can be found [here](https://github.com/bpucker/teaching/tree/master/MB09_PlantBiochemistryBioinformatics).

<a href="https://github.com/bpucker/teaching/tree/master/MB09_PlantBiochemistryBioinformatics">
<img alt="Digitalis flowers" src="https://github.com/bpucker/teaching/blob/master/MB09_PlantBiochemistryBioinformatics/digitalis_pictures.png?raw=true" width="20%" height="20%">
</a>


### (4.2) Applied Plant Genomics
Material used in this course can be found [here](https://github.com/bpucker/teaching/tree/master/GE31_AppliedPlantGenomics). Scripts and instructions of the bioinformatics component are available [here](https://github.com/bpucker/AppliedPlantGenomics).

<a href="https://github.com/bpucker/teaching/tree/master/GE31_AppliedPlantGenomics">
<img alt="ONT sequencing" src="https://www.tu-braunschweig.de/index.php?eID=dumpFile&t=p&p=625087&token=b5c0ad2fa83053e994c1789a280edb16e3078b1c" width="20%" height="20%">
</a>


### (4.3) Data Literacy in Plant Sciences (in preparation) 
Material used in this course can be found [here](https://github.com/bpucker/teaching/tree/master/GE32_DataLiteracyInPlantSciences). Scripts and instructions of the bioinformatics component are available [here](https://github.com/bpucker/DataLiteracyInPlantSciences).

### (4.4) iGEM @ TU Braunschweig ###
iGEM (international Genetically Engineered Machine) is the largest synthetic biology competition with over 300 international teams participating each year. [PuckerLab](https://www.tu-braunschweig.de/en/ifp/pbb) is hosting the [TU Braunschweig iGEM team 2022](https://www.tu-braunschweig.de/en/igem/2022).

<a href="https://www.tu-braunschweig.de/en/igem/2022">
<div>
<img alt="iGEM" src="https://github.com/bpucker/figures/blob/main/igem_logo.png" width="20%" height="20%">
</div>
</a>  


## (5) Project Specific Repositories

https://github.com/bpucker/ApiaceaeFNS1 (preprint available)

https://github.com/bpucker/CoExp (web server support)

https://github.com/bpucker/RNA-Seq_analysis (PEPD nature paper)

https://github.com/bpucker/pitaya (pitaya1+pitaya2)

https://github.com/bpucker/CaryoAnthoBlock (under review)

https://github.com/bpucker/LongReadWalker (standalone tool?; generate DOI)

https://github.com/bpucker/GKseq (add loreta link)

https://github.com/bpucker/variant_calling (NAVIP + mapper compraison)

https://github.com/bpucker/yam (yam paper)

https://github.com/bpucker/ncss2018 (splice site scripts)

https://github.com/bpucker/At7

https://github.com/bpucker/Nd1_PacBio

https://github.com/bpucker/PEPD (PEPD structure preprint)


## References
Please find a full list of our publications on the [@PuckerLab website](https://www.tu-braunschweig.de/en/ifp/pbb/publications).


## Contact
Do you have questions or are you interested in a collaboration? Please get in touch via [email](https://www.tu-braunschweig.de/en/ifp/pbb/contact).
